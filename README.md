[![Codacy Badge](https://app.codacy.com/project/badge/Grade/17d587089c964e02a4371e195b922fd6)](https://www.codacy.com/gl/paulo.morais1/Ping.API/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=paulo.morais1/Ping.API&amp;utm_campaign=Badge_Grade)
# Ping.API
<b>V1</b>
Rota para /ping criada

Para realização de testes adicionar as seguintes steps:

```
- task: UseDotNet@2
  inputs:
    version: '5.0.x'
    includePreviewVersions: true # Required for preview versions
```

  
```
- task: DotNetCoreCLI@2
  displayName: 'dotnet build'
  inputs:
    command: 'build'
    configuration: $(buildConfiguration)
```

  
```
- task: DotNetCoreCLI@2
  displayName: 'dotnet test'
  inputs:
    command: 'test'
    arguments: '--configuration $(buildConfiguration) /p:CollectCoverage=true /p:CoverletOutputFormat=cobertura /p:CoverletOutput=$(Build.SourcesDirectory)/TestResults/Coverage/'
    publishTestResults: true
    projects: 'MyTestLibrary' # update with your test project directory
```

  
```
- task: PublishCodeCoverageResults@1
  displayName: 'Publish code coverage report'
  inputs:
    codeCoverageTool: 'Cobertura'
    summaryFileLocation: '$(Build.SourcesDirectory)/**/coverage.cobertura.xml'
```

