﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ping.API.Models.Interfaces;
using System;
using System.Net;

namespace Ping.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PingController : ControllerBase
    {
        private readonly ILogger<PingController> _logger;
        private readonly IOperationScoped _operationScoped;

        public PingController(ILogger<PingController> logger, IOperationScoped operationScoped)
        {
            _logger = logger;
            _operationScoped = operationScoped;
        }

        [HttpGet]
        [ProducesResponseType(typeof(object), (int)HttpStatusCode.OK)]
        public IActionResult Get()
        {
            _logger.LogInformation($"Chamada de ping realizada ID:{_operationScoped.OperationId }");
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            int secondsSinceEpoch = (int)t.TotalSeconds;

            return Ok(new
            {
                ts = DateTime.Now,
                epoch = secondsSinceEpoch
            });
        }
    }
}