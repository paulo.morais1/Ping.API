using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Middleware.Trace;
using Ping.API.Handlers;
using Ping.API.Models;
using Ping.API.Models.Interfaces;
using System;

namespace Ping.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Ping.API", Version = "v1" });
            });

            services.AddHttpContextAccessor();

            services.AddHttpClient<Client>((sp, httpClient) =>
            {
                var httpRequest = sp.GetRequiredService<IHttpContextAccessor>().HttpContext.Request;
                httpClient.BaseAddress = new Uri(UriHelper.BuildAbsolute(httpRequest.Scheme,
                                                                 httpRequest.Host, httpRequest.PathBase));
                httpClient.Timeout = TimeSpan.FromSeconds(5);
            });
            services.AddScoped<IOperationScoped, OperationScoped>();
            services.AddTransient<OperationHandler>();
            services.AddTransient<OperationResponseHandler>();

            services.AddHttpClient("Operation")
                .AddHttpMessageHandler<OperationHandler>()
                .AddHttpMessageHandler<OperationResponseHandler>()
                .SetHandlerLifetime(TimeSpan.FromSeconds(5));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ping.API v1"));
            }
            app.UseMiddleware<LogHeaderMiddleware>();
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}