﻿namespace Ping.API.Models.Interfaces
{
    public interface IOperationScoped
    {
        string OperationId { get; }
    }
}