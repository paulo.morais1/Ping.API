﻿using Ping.API.Models.Interfaces;
using System;

namespace Ping.API.Models
{
    public class OperationScoped : IOperationScoped
    {
        public string OperationId { get; } = Guid.NewGuid().ToString()[^4..];
    }
}