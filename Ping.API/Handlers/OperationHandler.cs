﻿using Ping.API.Models.Interfaces;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Ping.API.Handlers

{
    public class OperationHandler : DelegatingHandler
    {
        private readonly IOperationScoped _operationService;

        public OperationHandler(IOperationScoped operationScoped)
        {
            _operationService = operationScoped;
        }

        protected async override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            request.Headers.Add("X-OPERATION-ID", _operationService.OperationId);

            return await base.SendAsync(request, cancellationToken);
        }
    }
}