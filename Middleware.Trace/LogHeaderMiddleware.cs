﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Middleware.Trace
{
    public class LogHeaderMiddleware
    {
        private readonly RequestDelegate _next;

        public LogHeaderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var header = context.Request.Headers["X-OPERATION-ID"];
            string sessionId;

            if (header.Count > 0)
            {
                sessionId = header[0];
            }
            else
            {
                Range startIndex = ^4..;
                sessionId = Guid.NewGuid().ToString()[startIndex];
            }

            context.Items["X-OPERATION-ID"] = sessionId;
            await _next(context);
        }
    }
}