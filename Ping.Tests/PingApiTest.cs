using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Ping.API;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Ping.Tests
{
    public class PingApiTest
    {
        public readonly HttpClient _client;

        public PingApiTest()
        {
            var server = new TestServer(new WebHostBuilder()
                .UseEnvironment("Development")
                .UseStartup<Startup>());
            _client = server.CreateClient();
        }

        [Theory]
        [InlineData("GET")]
        public async Task GetResponsePing(string method)
        {
            //arrange
            var request = new HttpRequestMessage(new HttpMethod(method), "/ping");

            //act
            var response = await _client.SendAsync(request);

            //assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}